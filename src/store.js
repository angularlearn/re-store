import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers';

const logMiddleware = ({ getState }) => (next) => (action) => { //"next" is an alias for dispatch
    console.log(action.type, getState());
    return next(action);
}

const stringMiddleware = () => (next) => (action) => {
    if (typeof action === 'string') {
        return next({
            type: action
        });
    }

    return next(action);
}

const store = createStore(reducer, applyMiddleware(thunkMiddleware, stringMiddleware, logMiddleware));
store.dispatch('HELLO_WORLD');

const delayedActionCreatore = (timeout) => (dispatch) => {
    setTimeout(() => dispatch({
        type: 'DELAYED_ACTION'
    }), timeout);
}

store.dispatch(delayedActionCreatore(3000));

export default store;
